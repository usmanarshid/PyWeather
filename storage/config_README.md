## Configuration File Readme
For PyWeather 1.0.3

This document will tell you all about the various configuration options of PyWeather (what they mean), and the defaults.

## GEOCODER API section
This section controls the geocdoer and custom options.

### customkey_enabled
This controls if a custom Google geocoder API key is enabled. By default, this is False.

If this value is False, the ArcGIS geocoder will be used instead.

### customkey
This is where you'd put your custom Google geocoder API key that you retrieved from Google. By default, this is None.

### customkey_w3w
THis is where you'd put your custom what3words geocoder API key that you retrieved from what3words. By default, this is None.

### skipverification
This controls if the verification of a custom geocoder API key should be skipped. By default, this is False.

## FAVORITE LOCATIONS section
This section controls favorite locations, and data for favorite locations.

### enabled
This controls if the favorite locations feature is enabled. By default, this is True.

### favloc1-5
This stores data for the favorite locations 1 - 5. By default, all of these are None.

### favloc1-5_data
This stores extra data for favorite locations 1 - 5 (like airport name, etc). By default, all of these are none.

## HURRICANE section
This section controls options for hurricane nearest city data.

### enablenearestcity
This controls if the nearest city feature is enabled on the summary screen only. By default, this is False.

### enablenearestcity_forecast
This controls if the nearest city feature is enabled on the forecast screens only. By default, this is False.

### api_username
This controls the API username from Geonames.org for nearest city API access. By default, this is pyweather_proj

### nearestcitysize
This controls what size the nearest city has to be for it to show up. Options are small (1,000 population or more), medium (5,000 population or more), and large (15,000 population or more). By default, this is medium.

## FIRSTINPUT
This controls what types of queries you would like to enable in PyWeather.

### geoipservice_enabled
This controls if the current location service for PyWeather is enabled. To preserve privacy, by default, this is False.

ipapi.co is used to provide the service.

### allow_pwsqueries
This controls if PWS queries are enabled in PyWeather. By default, this is True.

### allow_airportqueries
This controls if airport queries are enabled in PyWeather. By default, this is True.

### allow_latlonqueries
This controls if direct latitude/longitude queries are enabled in PyWeather. By default, this is True.

### allow_w3wqueries
This controls if what3words queries are enabled in PyWeather. By default, this is False.

## SUMMARY
This controls what data shows up on the summary screen of PyWeather.

### sundata_summary
This controls if sunrise/set data is shown on the summary screen. By default, this is False. Enabling this uses 1 extra API call at boot.

### almanac_summary
This controls if almanac data is shown on the summary screen. By default, this is False. Enabling this uses 1 extra API call at boot.

### showalertsonsumary
This controls if alerts data is shown on the summary screen. By default, this is True. Enabling this uses 1 extra API call at boot.

### showtideonsummary
This controls if tide data is shown on the summary screen. By default, this is False. Enabling this uses 1 extra API call at boot.

### showyesterdayonsummary
This controls if the previous day's weather data is shown on the summary screen. By default, this is False. Enabling this uses 1 extra API call at boot.

## VERBOSITY
This section controls the various verbosity toggles throughout PyWeather.

### verbosity
This controls if verbosity is enabled in the main script. Useful for debugging. By default, this is False.

### json_verbosity
This controls if full .json files are printed in the main script (during boot & refresh). Useful for debugging. By default, this is False.

**Note: VERBOSITY/verbosity must be True for this option to work.**

**WARNING: If your terminal wraps text, enabling this will output hundreds of lines of raw data to your terminal.**

### setup_verbosity
This controls if verbosity is enabled in the setup script. Useful for debugging. By default, this is False.

### setup_jsonverbosity
This controls if full .json files are printed in the setup script (during various checks). Useful for debugging. By default, this is False.

**Note: VERBOSITY/setup_verbosity must be True for this option to work.**

**Note: If your terminal wraps text, enabling this will output a few dozen lines of raw data to your terminal.**

### updater_verbosity
This controls if verbosity is enabled in the updater script. Useful for debugging. By default, this is False.

### updater_jsonverbosity
This controls if full .json files are printed in the setup script (during update checks). Useful for debugging. By default, this is False.

**Note: VERBOSITY/updater_verbosity must be True for this option to work.**

**Note: If your terminal wraps text, enabling this will output about a dozen lines of raw data to your terminal.**

## TRACEBACK
This section controls the various traceback toggles throughout PyWeather.

### tracebacks
This controls if full tracebacks are printed in the main script. Useful for debugging & bug reporting. By default, this is False.

### setup_tracebacks
This controls if full tracebacks are printed in the setup script. Useful for debugging & bug reporting. By default, this is False.

### updater_tracebacks
This controls if full tracebacks are printed in the updater script. Useful for debugging & bug reporting. By default, this is False.

## UI section
This section controls various aspects of the UI.

### show_entertocontinue
This option controls if "Enter to continue" prompts should appear in PyWeather. By default, this is True.

### show_completediterations
This option controls if a field showing how many iterations have completed should show up under every data block in PyWeather. By default, this is False.

### extratools_enabled
This option controls if diagnostic tools for PyWeather should be enabled. By default, this is False.

### hourlyinfo_iterations
This option controls how many iterations of hourly information PyWeather should go through before prompting to continue. By default, this is 2.

**Note: UI/show_entertocontinue must be True for this option to work.**

### forecastinfo_iterations
This option controls how many iterations of forecast information PyWeather should go through before prompting to continue. By default, this is 3.

**Note: UI/show_entertocontinue must be True for this option to work.**

### hurricaneinfo_iterations
This option controls how many iterations of hurricane information PyWeather should go thorugh before prompting to continue. By default, this is 2.

**Note: UI/show_entertocontinue must be True for this option to work.**

### tideinfo_iterations
This option controls how many iterations of tide information PyWeather should go through before prompting to continue. By default, this is 6.

**Note: UI/show_entertocontinue must be True for this option to work.**

### historicalhourly_iterations
This option controls how many iterations of historical hourly information PyWeather should go through before prompting to continue. By default, this is 2.

**Note: UI/show_entertocontinue must be True for this option to work.**

### yesterdayhourly_iterations
This option controls how many iterations of yesterday hourly information PyWeather should go through before prompting to continue. By default, this is 3.

**Note: UI/show_entertocontinue must be True for this option to work.**

### alertsus_iterations
This option controls how many iterations of US alerts information PyWeather should go through before prompting to continue. By default, this is 1.

**Note: UI/show_entertocontinue must be True for this option to work.**

### alertseu_iterations
This option controls how many iterations of EU alerts information PyWeather should go through before prompting to continue. By default, this is 2.

**Note: UI/show_entertocontinue must be True for this option to work.**

## PREFETCH section
This section controls various aspects of prefetching during PyWeather boot.

### 10dayfetch_atboot
This option controls if 10-day hourly information should be fetched alongside the usual 36-hour hourly information at boot. Enabling this will use 1 extra API call at boot. By default, this is False.

### yesterdaydata_atboot
This option controls if yesterday weather information should be fetched at boot. Enabling this will use 1 extra API call at boot. By default, this is False.

### hurricanedata_atboot
This option controls if hurricane weather information should be fetched at boot. Enabling this will use 1 extra API call at boot. By default, this is False.

## UPDATER section
This section controls various aspects of the updater portion of PyWeather.

### autocheckforupdates
This controls if PyWeather should check for updates at boot. Enabling this will increase PyWeather's initial load time. By default, this is False.

### show_updaterreleasetag
This controls if PyWeather should show the Git updater tag for the latest version of PyWeather. By default, this is False.

### showreleasenotes
This controls if PyWeather should prompt you to view the release notes in the updater, when PyWeather is not up-to-date. By default, this is True.

### showreleasenotes_uptodate
This controls if PyWeather should prompt you to view the release notes in the updater, even when PyWeather is up-to-date. By default, this is False.

### shownewversionreleasedate
This controls if PyWeather should show a field estimating when the next version of PyWeather should be released, when PyWeather is up-to-date. By default, this is True.

## KEYBACKUP section
This section controls aspects of the API key backup functionality.

### savedirectory
This controls which directory the backup API key is located in. Folder dividers are marked with "//". In addition, "//" must be placed at the end of the string.

It is not recommended you use "//" as PyWeather may have issues with locating your backup API key.

By default, this is "backup//".

## PYWEATHER BOOT section
This section controls aspects of PyWeather bootup functionality.

### validateapikey
This controls if PyWeather should validate your API key on boot. If your primary API key is invalid, PyWeather will look for your backup key. If it is valid, PyWeather will use the key. If it is invalid, PyWeather will not be able to run.

This uses 1 extra API call at boot, and enabling this will increase PyWeather's initial load time. By default, this is True.

## USER section
This section controls if the configuration file has been provisioned.

### configprovisioned
This is used as a test key to see if a user's config file has been provisioned. The value stored in this key does not matter. However, it is recommended you keep it set to True.

## CACHE section
This section controls various aspects of the PyWeather caching system.

### enabled
This controls if the PyWeather caching system is enabled. By default, this is True.

### alerts_cachedtime
This controls how long (in minutes) alerts data can be stored for, before it is refreshed when you want to view alerts data. By default, this is 5.

**Note: CACHE/enabled must be True for this option to work.**

### current_cachedtime
This controls how long (in minutes) current weather data can be stored for, before it is refreshed when you want to view current weather data. By default, this is 10.

**Note: CACHE/enabled must be True for this option to work.**

### 36hourly_cachedtime
This controls how long (in minutes) 36-hour hourly data can be stored for, before it is refreshed when you want to view 36-hour hourly data. By default, this is 60.

**Note: CACHE/enabled must be True for this option to work.**

### tendayhourly_cachedtime
This controls how long (in minutes) 10-day hourly data can be stored for, before it is refreshed when you want to view 10-day hourly data. By default, this is 60.

**Note: CACHE/enabled must be True for this option to work.**

### forecast_cachedtime
This controls how long (in minutes) forecast data can be stored for, before it is refreshed when you want to view forecast data. By default, this is 60.

**Note: CACHE/enabled must be True for this option to work.**

### almanac_cachedtime
This controls how long (in minutes) almanac data can be stored for, before it is refreshed when you want to view almanac data. By default, this is 240.

**Note: CACHE/enabled must be True for this option to work.**

### sundata_cachedtime
This controls how long (in minutes) sunrise/set, moonrise/set & moon phase data can be stored for, before it is refreshed when you want to view sunrise/set, moonrise/set & moon phase data. By default, this is 480.

**Note: CACHE/enabled must be True for this option to work.**

### tide_cachedtime
This controls how long (in minutes) tide data can be stored for, before it is refreshed when you want to view tide data. By default, this is 480.

**Note: CACHE/enabled must be True for this option to work.**

### hurricane_cachedtime
This controls how long (in minutes) hurricane data can be stored for, before it is refreshed when you want to view hurricane data. By default, this is 180.

**Note: CACHE/enabled must be True for this option to work.**

### yesterday_cachedtime
This controls how long (in minutes) yesterday's weather data can be stored for, before it is refreshed when you want to view yesterday's weather data. By default, this is 720.

**Note: CACHE/enabled must be True for this option to work.**

## RADAR GUI section
This section controls various aspects of the radar portion of PyWeather.

### radar_imagesize
This option controls the size of the radar image. There are five options to select from with different resolutions:
* extrasmall - 320x240
* small - 480x360
* normal - 640x480
* large - 960x720
* extralarge - 1280x960

Selecting a larger radar image size will increase PyWeather data usage, and slow down how fast images are refreshed.

By default, this is "normal".

### bypassconfirmation
This option controls if PyWeather should automatically bypass the experimental warning when entering the radar portion of PyWeather. By default, this is False.