# PyWeather Updater - Version 1.0.3
# (c) 2017 - 2018, o355

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import configparser
import traceback
import sys
import logging
import json
import os
import zipfile
import hashlib

buildnumber = 103
buildversion = "1.0.3"

try:
    import requests
except ImportError:
    print("When attempting to import the library requests, we ran into an import error.",
          "Please make sure that requests is installed.",
          "Press enter to exit.", sep="\n")
    input()
    sys.exit()

try:
    import click
except ImportError:
    print("When attempting to import the library click, we ran into an import error.",
          "Please make sure that click is installed. Press enter to exit.", sep="\n")
    input()
    sys.exit()

try:
    from colorama import init, Fore, Style
    import colorama
except ImportError:
    print("When attempting to import the library colorama, we ran into an import error.",
          "Please make sure that colorama is installed.",
          "Press enter to exit.", sep="\n")
    input()
    sys.exit()

try:
    from halo import Halo
except ImportError:
    print("When attempting to import the library halo, we ran into an import error.",
          "Please make sure that halo is installed.",
          "Press enter to exit.", sep="\n")
    input()
    sys.exit()

try:
    versioninfo = open('updater//versioninfo.txt').close()
except:
    print("An error occurred while attempting to read the version information file.",
          "This error is likely being caused by launching PyWeather outside of the pyweather folder.",
          "Please try launching PyWeather when inside the PyWeather folder, and make sure that updater/versioninfo.txt",
          "is accessible. You can recreate the version info file by running setup.py or configsetup.py.", sep="\n")
    sys.exit()

config = configparser.ConfigParser()
config.read('storage//config.ini')


try:
    configprovisioned = config.getboolean('USER', 'configprovisioned')
except:
    print("We ran into an error. Full traceback below.")
    traceback.print_exc()
    # Not doing a separate script launch. Doing sys.exit in that script would just continue
    # PyWeather. We ask the user to run the script themselves.
    print("This isn't good. Your config file isn't provisioned.",
          "If you're new to PyWeather, you should run the setup script.",
          "Otherwise, try launching configsetup.py, which is available in the",
          "storage folder. Press enter to exit.", sep="\n")
    input()
    sys.exit()

logger = logging.getLogger(name='pyweather_v1.0.3')
logformat = '%(asctime)s | %(levelname)s | %(message)s'
logging.basicConfig(format=logformat)

configerrorcount = 0

try:
    verbosity = config.getboolean('VERBOSITY', 'updater_verbosity')
except:
    print("When attempting to load your configuration file, an error occurred.",
          "VERBOSITY/verbosity failed to load. Defaulting to False.", sep="\n")
    configerrorcount += 1
    verbosity = False

try:
    jsonVerbosity = config.getboolean('VERBOSITY', 'updater_jsonverbosity')
except:
    print("When attempting to load your configuration file, an error occurred.",
          "VERBOSITY/json_verbosity failed to load. Defaulting to False.", sep="\n")
    configerrorcount += 1
    jsonVerbosity = False

try:
    tracebacksEnabled = config.getboolean('TRACEBACK', 'updater_tracebacks')
except:
    print("When attempting to load your configuration file, an error occurred.",
          "TRACEBACK/tracebacks failed to load. Defaulting to False.", sep="\n")
    configerrorcount += 1
    tracebacksEnabled = False

try:
    showUpdaterReleaseNotes = config.getboolean('UPDATER', 'showReleaseNotes')
except:
    print("When attempting to load your configuration file, an error occurred.",
          "UPDATER/showReleaseNotes failed to load. Defaulting to True.", sep="\n")
    configerrorcount += 1
    showUpdaterReleaseNotes = True

try:
    showUpdaterReleaseNotes_uptodate = config.getboolean('UPDATER', 'showReleaseNotes_uptodate')
except:
    print("When attempting to load your configuration file, an error occurred.",
          "UPDATER/showReleaseNotes_uptodate failed to load. Defaulting to False.", sep="\n")
    configerrorcount += 1
    showUpdaterReleaseNotes_uptodate = False

try:
    showNewVersionReleaseDate = config.getboolean('UPDATER', 'showNewVersionReleaseDate')
except:
    print("When attempting to load your configuration file, an error occurred.",
          "UPDATER/showNewVersionReleaseDate failed to load. Defaulting to True.", sep="\n")
    configerrorcount += 1
    showNewVersionReleaseDate = True

try:
    user_showUpdaterReleaseTag = config.getboolean('UPDATER', 'show_updaterReleaseTag')
except:
    print("When attempting to load your configuration file, an error occurred.",
          "UPDATER/show_updaterReleaseTag failed to load. Defaulting to False.", sep="\n")
    configerrorcount += 1
    user_showUpdaterReleaseTag = False

if configerrorcount >= 1:
    print("", "When trying to load your configuration file, error(s) occurred.",
          "Try making sure that there are no typos in your config file, and try setting values",
          "in your config file to the default values as listed above. If all else fails, try using",
          "configsetup.py to set all config options to their defaults. If issues still occur,",
          "report the bug on GitLab.", sep="\n")

spinner = Halo(text='Checking for updates...', spinner='line')
spinner.start()

# Set the logger levels by design. Critical works as a non-verbosity
# option, as I made sure not to have any critical messages. - Section 7
if verbosity:
    logger.setLevel(logging.DEBUG)
elif tracebacksEnabled:
    logger.setLevel(logging.ERROR)
else:
    logger.setLevel(logging.CRITICAL)


logger.debug("Configuration options listed below:")
logger.debug("verbosity: %s ; jsonVerbosity: %s" % (verbosity, jsonVerbosity))
logger.debug("tracebacksEnabled: %s ; showUpdaterReleaseNotes: %s" % (tracebacksEnabled, showUpdaterReleaseNotes))
logger.debug("showUpdaterReleaseNotes_uptodate: %s ; showNewVersionReleaseDate: %s" %
      (showUpdaterReleaseNotes_uptodate, showNewVersionReleaseDate))
logger.debug("user_showUpdaterReleaseTag: %s" % user_showUpdaterReleaseTag)


def printException():
    # We use tracebacksEnabled here, as it just worked.
    if tracebacksEnabled is True:
        print("Here's the full traceback (for bug reporting):")
        traceback.print_exc()


def printException_loggerwarn():
    # Same idea. If the print_exc was in just logger.warn, it'd print even
    # if verbosity was disabled.
    if verbosity is True:
        logger.warning("Snap! We hit a non-critical error. Here's the traceback.")
        traceback.print_exc()





# Request updater JSON
try:
    updaterJSON = requests.get("https://gitlab.com/o355/PyWeather/raw/master/updater/versioncheck_v2.json")
    logger.debug("updaterJSON fetched with end result: %s" % updaterJSON)
except:
    spinner.fail(text="Failed to check for updates! (error occurred while fetching updater API)")
    print("")
    logger.warning("Couldn't check for updates! Internet connection issue?")
    print(Fore.RED + Style.BRIGHT + "When attempting to contact the updater API, PyWeather ran into an error.",
        Fore.RED + Style.BRIGHT + "Make sure that gitlab.com is unblocked if you're on a filtered network,",
        Fore.RED + Style.BRIGHT + "and make sure you have a working internet connection. Press enter to exit.", sep="\n")
    printException()
    input()
    sys.exit()

updaterJSON = json.loads(updaterJSON.text)
if jsonVerbosity is True:
    logger.debug("updaterJSON: %s" % updaterJSON)
else:
    logger.debug("updaterJSON loaded.")

updater_releaseNotesURL = updaterJSON['branch']['stable']['releasenotesurl']
logger.debug("updater_releaseNotesURL: %s" % updater_releaseNotesURL)

try:
    updater_releasenotes = requests.get(updater_releaseNotesURL)
    logger.debug("updater_releasenotes fetched with end result: %s" % updater_releasenotes)
except:
    spinner.fail("Failed to check for updates (error occurred while fetching release notes)")
    print("")
    logger.warning("Couldn't fetch release notes, internet connection issue?")
    print(Fore.RED + Style.BRIGHT + "When attempting to fetch the release notes for PyWeather, an error occurred.",
          Fore.RED + Style.BRIGHT + "Make sure that gitlab.com is unblocked if you're on a filtered network,",
          Fore.RED + Style.BRIGHT + "and make sure you have a working internet connection. Press enter to exit.", sep="\n")
    printException()
    input()
    sys.exit()

# Begin reading updater information
updater_latestBuildNumber = float(updaterJSON['branch']['stable']['latestbuild'])
updater_latestVersion = updaterJSON['branch']['stable']['latestversion']
updater_latestVersionTag = updaterJSON['branch']['stable']['latestversiontag']
updater_latestDownloadURL = updaterJSON['branch']['stable']['latesturl']
updater_latestDirlessDownloadURL = updaterJSON['branch']['stable']['latestdirlessurl']
updater_latestFilename = updaterJSON['branch']['stable']['latestfilename']
updater_latestDirlessFilename = updaterJSON['branch']['stable']['latestdirlessfilename']
updater_latestReleaseDate = updaterJSON['branch']['stable']['releasedate']
updater_nextVersionReleaseDate = updaterJSON['branch']['stable']['nextversionreleasedate']
updater_latestSize = updaterJSON['branch']['stable']['size']
updater_latestSHA1DirlessSum = updaterJSON['branch']['stable']['sha1sum-dirless']
updater_latestSHA256DirlessSum = updaterJSON['branch']['stable']['sha256sum-dirless']
updater_showDepNotice = bool(updaterJSON['branch']['stable']['updater_depNoticeShow'])
updater_depNoticeText = updaterJSON['branch']['stable']['updater_depNoticeMessage']

logger.debug("updater_latestBuildNumber: %s ; updater_latestVersion: %s" %
             (updater_latestBuildNumber, updater_latestVersion))
logger.debug("updater_latestVersionTag: %s ; updater_latestDownloadURL: %s" %
             (updater_latestVersionTag, updater_latestDownloadURL))
logger.debug("updater_latestDirlessDownloadURL: %s ; updater_latestFilename: %s" %
             (updater_latestDirlessDownloadURL, updater_latestFilename))
logger.debug("updater_latestDirlessFilename: %s ; updater_latestReleaseDate: %s" %
             (updater_latestDirlessFilename, updater_latestReleaseDate))
logger.debug("updater_nextVersionReleaseDate: %s ; updater_latestSize: %s" %
             (updater_nextVersionReleaseDate, updater_latestSize))
logger.debug("updater_latestSHA1DirlessSum: %s ; updater_latestSHA256DirlessSum: %s" %
             (updater_latestSHA1DirlessSum, updater_latestSHA256DirlessSum))
logger.debug("updater_showDepNotice: %s ; updater_depNoticeText: %s" %
             (updater_showDepNotice, updater_depNoticeText))

# And print out everything in the logger

spinner.stop()
spinner.clear()
if updater_showDepNotice is True:
    print(Fore.YELLOW + Style.BRIGHT + updater_depNoticeText)
if buildnumber >= updater_latestBuildNumber:
    logger.info("PyWeather is up to date. Local build num (%s) >= latest build num (%s)." %
                (buildnumber, updater_latestBuildNumber))
    # We all need a little happiness in our lives so the smiley face stays
    print(Fore.YELLOW + Style.BRIGHT + "Your copy of PyWeather is up to date! :)")
    print(Fore.YELLOW + Style.BRIGHT + "You have PyWeather version: " + Fore.CYAN + Style.BRIGHT + buildversion)
    print(Fore.YELLOW + Style.BRIGHT + "The latest PyWeather version is: " +
          Fore.CYAN + Style.BRIGHT + updater_latestVersion)
    if user_showUpdaterReleaseTag is True:
        print(Fore.YELLOW + Style.BRIGHT + "The latest release tag is: " +
              Fore.CYAN + Style.BRIGHT + updater_latestVersionTag)
    if showNewVersionReleaseDate is True:
        if updater_nextVersionReleaseDate != "Not available":
            print(Fore.YELLOW + Style.BRIGHT + "A new version of PyWeather should come out in: " +
                  Fore.CYAN + Style.BRIGHT + updater_nextVersionReleaseDate)
    if showUpdaterReleaseNotes_uptodate is True:
        # Release notes keep getting longer, so ask the user to print the release notes if they want to.
        print("")
        print(Fore.YELLOW + Style.BRIGHT + "Would you like to see the release notes for this release of PyWeather?",
              Fore.YELLOW + Style.BRIGHT + "If so, enter 'yes' at the input below. Otherwise, press enter to continue.", sep="\n")
        updater_showReleaseNotesInput = input("Input here: ").lower()
        logger.debug("updater_showReleaseNotesInput: %s" % updater_showReleaseNotesInput)
        if updater_showReleaseNotesInput == "yes":
            print(Fore.YELLOW + Style.BRIGHT + "Here's the release notes for this release:")
            print(updater_releasenotes.text)

    print("")
    print(Fore.YELLOW + Style.BRIGHT + "Press enter to exit.")
    input()
    sys.exit()
elif buildnumber < updater_latestBuildNumber:
    logger.info("PyWeather is not up to date. Local build number (%s) < latest build number (%s)" %
                (buildnumber, updater_latestBuildNumber))
    print("")
    print(Fore.YELLOW + Style.BRIGHT + "Your copy of PyWeather isn't up to date! :(")
    print(Fore.YELLOW + Style.BRIGHT + "You have PyWeather version: " + Fore.CYAN + Style.BRIGHT + buildversion)
    print(Fore.YELLOW + Style.BRIGHT + "The latest PyWeather version is: " +
          Fore.CYAN + Style.BRIGHT + updater_latestVersion)
    print(Fore.YELLOW + Style.BRIGHT + "And it was released on: "
          + Fore.CYAN + Style.BRIGHT + updater_latestReleaseDate)
    print(Fore.YELLOW + Style.BRIGHT + "The update size: "
          + Fore.CYAN + Style.BRIGHT + updater_latestSize)
    if user_showUpdaterReleaseTag is True:
        print(Fore.YELLOW + Style.BRIGHT + "The latest release tag is: " +
              Fore.CYAN + Style.BRIGHT + updater_latestVersionTag)
    if showUpdaterReleaseNotes is True:
        print("")
        print(
            Fore.YELLOW + Style.BRIGHT + "Would you like to see the release notes for this release of PyWeather?",
            Fore.YELLOW + Style.BRIGHT + "If so, enter 'yes' at the input below. Otherwise, press enter to continue.",
            sep="\n")
        updater_showReleaseNotesInput = input("Input here: ").lower()
        logger.debug("updater_showReleaseNotesInput: %s" % updater_showReleaseNotesInput)
        if updater_showReleaseNotesInput == "yes":
            print(Fore.YELLOW + Style.BRIGHT + "Here's the release notes for this release:")
            print(updater_releasenotes.text)

print("")
print(Fore.YELLOW + Style.BRIGHT + "Would you like to update PyWeather to the latest version? Yes or No.")
print(Fore.YELLOW + Style.BRIGHT + "If you would like to use the old .zip download method, Enter 'oldyes'.")
updateconfirmation = input("Input here: ").lower()
logger.debug("updateconfirmation: %s" % updateconfirmation)

if updateconfirmation == "yes" or updateconfirmation == "oldyes":
    logger.debug("We are updating PyWeather...")
elif updateconfirmation == "no":
    print(Fore.YELLOW + Style.BRIGHT + "Not updating PyWeather. Press enter to exit.")
    input()
    sys.exit()
else:
    print(Fore.YELLOW + Style.BRIGHT + "Could not understand your input. Not updating PyWeather. Press enter to exit.")
    input()
    sys.exit()

# Updater steps: Download, verify, unpack, run configupdate.py
print(Fore.YELLOW + Style.BRIGHT + "Now updating PyWeather. Please wait, this may take a moment.")

# Download the package

# If we're running the old updater, run all the code here. It's under 100 lines.

if updateconfirmation == "oldyes":
    try:
        updatepackage = requests.get(updater_latestDownloadURL, stream=True, timeout=20)
    except requests.exceptions.ConnectionError:
        print("")
        print(Fore.RED + Style.BRIGHT + "When attempting to start the download of the update, an error",
              Fore.RED + Style.BRIGHT + "occurred. Make sure that you have an internet connection, and that",
              Fore.RED + Style.BRIGHT + "gitlab.com is unblocked on your network. Press enter to exit.", sep="\n")
        printException()
        input()
        sys.exit()

    # Get the total length of the file for the progress bar
    updater_package_totalLength = int(updatepackage.headers.get('content-length'))
    updater_package_totalLength = int(updater_package_totalLength / 1024)
    logger.debug("updater_package_totalLength: %s" % updater_package_totalLength)

    with click.progressbar(length=updater_package_totalLength, label='Downloading') as bar:
        with open(updater_latestFilename, 'wb') as f:
            try:
                for chunk in updatepackage.iter_content(chunk_size=1024):
                    bar.update(1)
                    if chunk:
                        # I am truly sorry for how indented this is.
                        f.write(chunk)
                        f.flush()
            except requests.exceptions.ConnectionError:
                print("")
                print(Fore.RED + Style.BRIGHT + "When downloading update data, an error occurred.",
                      Fore.RED + Style.BRIGHT + "Please make sure you have an internet connection, and that",
                      Fore.RED + Style.BRIGHT + "gitlab.com is unblocked on your network. Press enter to exit.", sep="\n")
                printException()
                input()
                sys.exit()
    print("")
    print(
        Fore.YELLOW + Style.BRIGHT + "The latest version of PyWeather has been downloaded to the base directory of PyWeather,",
        Fore.YELLOW + Style.BRIGHT + "and has been saved as: " + Fore.CYAN + Style.BRIGHT + updater_latestFilename +
        Fore.YELLOW + Style.BRIGHT + ".",
        Fore.YELLOW + Style.BRIGHT + "Press enter to exit.", sep="\n")
    input()
    sys.exit()

try:
    updatepackage = requests.get(updater_latestDirlessDownloadURL, stream=True, timeout=20)
except requests.exceptions.ConnectionError:
    print("")
    print(Fore.RED + Style.BRIGHT + "When attempting to start the download of the update, an error",
          Fore.RED + Style.BRIGHT + "occurred. Make sure that you have an internet connection, and that",
          Fore.RED + Style.BRIGHT + "gitlab.com is unblocked on your network. Press enter to exit.", sep="\n")
    printException()
    input()
    sys.exit()

# Get the total length of the file for the progress bar
updater_package_totalLength = int(updatepackage.headers.get('content-length'))
updater_package_totalLength = int(updater_package_totalLength / 1024)
logger.debug("updater_package_totalLength: %s" % updater_package_totalLength)

# Here's the actual updater loop
with click.progressbar(length=updater_package_totalLength, label='Downloading') as bar:
    with open(updater_latestDirlessFilename, 'wb') as f:
        try:
            for chunk in updatepackage.iter_content(1024):
                bar.update(1)
                if chunk:
                    # I am truly sorry for how indented this is.
                    f.write(chunk)
                    f.flush()
        except requests.exceptions.ConnectionError:
            print("")
            print(Fore.RED + Style.BRIGHT + "When downloading update data, an error occurred.",
                  Fore.RED + Style.BRIGHT + "Please make sure you have an internet connection, and that",
                  Fore.RED + Style.BRIGHT + "github.com is unblocked on your network. Press enter to exit.", sep="\n")
            printException()
            input()
            sys.exit()

# Begin verification of files

hash_sha1 = hashlib.sha1()
hash_sha256 = hashlib.sha256()

# Do the SHA1 hash
try:
    with open(updater_latestDirlessFilename, "rb") as f:
        # I did not Ctrl+C & Ctrl+V this off of Stack Overflow, trust me
        for chunk in iter(lambda: f.read(4096), b""):
            hash_sha1.update(chunk)
        # Get the SHA1 hash
        updater_package_sha1hash = hash_sha1.hexdigest()
        logger.debug("updater_package_sha1hash: %s" % updater_package_sha1hash)
except:
    print("")
    print(Fore.RED + Style.BRIGHT + "Failed to load file to do a checksum verification.",
          Fore.RED + Style.BRIGHT + "Cannot continue as the file may be corrupt/non-existant.",
          Fore.RED + Style.BRIGHT + "Press enter to exit.", sep="\n")
    printException()
    input()
    sys.exit()

# Verify the SHA1 hash/sum/whatever you call it
if updater_latestSHA1DirlessSum != updater_package_sha1hash:
    logger.warning("SHA1 VERIFICATION FAILED! Checksum mismatch.")
    logger.debug("Expected sum (%s) was not sum of the download data (%s)" %
                 (updater_latestSHA1DirlessSum, updater_package_sha1hash))
    print("")
    print(
        Fore.RED + Style.BRIGHT + "Failed to verify the download data, a hash mismatch occurred.",
        Fore.RED + Style.BRIGHT + "Please try again at another time, or use a different connection.",
        Fore.RED + Style.BRIGHT + "Press enter to exit.", sep="\n")
    input()
    sys.exit()
else:
    logger.debug("SHA1 sum verified.")
    logger.debug("Expected sum (%s) was the sum of the download data (%s)" %
                 (updater_latestSHA1DirlessSum, updater_package_sha1hash))

# Do the SHA256 hash
try:
    with open(updater_latestDirlessFilename, "rb") as f:
        # I did not Ctrl+C & Ctrl+V this off of Stack Overflow, trust me
        for chunk in iter(lambda: f.read(4096), b""):
            hash_sha256.update(chunk)
        # Get the SHA256 hash
        updater_package_sha256hash = hash_sha256.hexdigest()
        logger.debug("updater_sha256hash: %s" % updater_package_sha256hash)
except:
    print("")
    print(Fore.RED + Style.BRIGHT + "Failed to load file to do a checksum verification.",
          Fore.RED + Style.BRIGHT + "Cannot continue as the file may be corrupt/non-existant.",
          Fore.RED + Style.BRIGHT + "Press enter to exit.", sep="\n")
    printException()
    input()
    sys.exit()

# Verify the SHA256 hash/sum/whatever you call it
if updater_latestSHA256DirlessSum != updater_package_sha256hash:
    logger.warning("SHA256 VERIFICATION FAILED! Checksum mismatch.")
    logger.debug("Expected sum (%s) was not sum of the download data (%s)" %
                 (updater_latestSHA256DirlessSum, updater_package_sha256hash))
    print("")
    print(Fore.RED + Style.BRIGHT + "Failed to verify the download data, a hash mismatch occurred.",
          Fore.RED + Style.BRIGHT + "Please try again at another time, or use a different connection.",
          Fore.RED + Style.BRIGHT + "Press enter to exit.", sep="\n")
    input()
    sys.exit()
else:
    logger.debug("SHA256 sum verified.")
    logger.debug("Expected sum (%s) was the sum of the download data (%s)" %
                 (updater_latestSHA256DirlessSum, updater_package_sha256hash))

print("Update data verified. Extracting...")
try:
    zf = zipfile.ZipFile(updater_latestDirlessFilename)
except zipfile.BadZipFile:
    print("")
    print(Fore.RED + Style.BRIGHT + "Failed to load the updater .zip file, a bad zip file error occurred.",
          Fore.RED + Style.BRIGHT + "The updater package could be corrupt or has bad permissions.",
          Fore.RED + Style.BRIGHT + "Press enter to exit.", sep="\n")
    printException()
    input()
    sys.exit()
except:
    print("")
    print(Fore.RED + Style.BRIGHT + "An error occurred when loading the .zip file. The updater",
          Fore.RED + Style.BRIGHT + "could be corrupt, have bad permissions, or could have been deleted.",
          Fore.RED + Style.BRIGHT + "Press enter to exit.", sep="\n")
    printException()
    input()
    sys.exit()

try:
    for file in zf.namelist():
        zf.extract(file, "")
except:
    print("")
    print(Fore.RED + Style.BRIGHT + "An error ocurred when extracting the contents of the updater .zip",
          Fore.RED + Style.BRIGHT + "file. The updater could be corrupt, have bad permissions, or could",
          Fore.RED + Style.BRIGHT + "have been deleted. Press enter to exit.",
          sep="\n")
    printException()
    input()
    sys.exit()

print("Update extracted. Launching configuration updater...")

exec(open("configupdate.py").read())


# Delete the updater .zip file
print("")
print(
    Fore.YELLOW + Style.BRIGHT + "If you would like to, the now unnecessary updater file can be automatically deleted for you.",
    Fore.YELLOW + Style.BRIGHT + "Would you like to have the updater file %s deleted for you? Yes or No." % updater_latestDirlessFilename,
    sep="\n")
updater_deleteupdaterfile = input("Input here: ").lower()
logger.debug("updater_deleteupdaterfile: %s" % updater_deleteupdaterfile)
if updater_deleteupdaterfile == "yes":
    print(Fore.YELLOW + Style.BRIGHT + "Now deleting the updater file.")
    try:
        os.remove(updater_latestDirlessFilename)
    except:
        print(
            Fore.YELLOW + Style.BRIGHT + "Failed to delete the updater file. The file could no longer exist, have bad permissions,",
            Fore.YELLOW + Style.BRIGHT + "or be corrupt.", sep="\n")
elif updater_deleteupdaterfile == "no":
    print(Fore.YELLOW + Style.BRIGHT + "Not deleting the updater file.")
else:
    print(
        Fore.YELLOW + Style.BRIGHT + "Your input could not be understood. The updater file will not be deleted automatically.")

# We're now done, exit PyWeather
print(
    Fore.YELLOW + Style.BRIGHT + "PyWeather is now up-to-date. To complete the updater process, PyWeather needs to be shut down.",
    Fore.YELLOW + Style.BRIGHT + "Please press enter to shut down PyWeather.", sep="\n")
input()
sys.exit()
